#ifndef _TUPLEOPERATION_HPP_
#define _TUPLEOPERATION_HPP_
#include <type_traits>
#include <array>

namespace AZ
{

template <typename... Types>
struct VariadicSize
{
	static constexpr size_t value = sizeof...( Types );
};

template <typename... Types>
constexpr static size_t VariadicSize_v = VariadicSize<Types...>::value;

template <std::size_t ...>
struct IndexSequence
{
};

template <std::size_t N, std::size_t ... Next>
struct IndexSequenceHelper
	: public IndexSequenceHelper<N - 1U, N - 1U, Next...>
{
};

template < std::size_t ... Next >
struct IndexSequenceHelper<0U, Next ... >
{
	using type = IndexSequence<Next ... >;
};

template <std::size_t N>
using MakeIndexSequence_t = typename IndexSequenceHelper<N>::type;

// forward declaration
template< typename Sequence, typename... Types >
struct BaseTuple;

template< typename... Types >
struct Tuple;

template<typename T>
struct IsBaseTuple
	:	std::false_type
{
};

template< size_t... Indices, typename... Types>
struct IsBaseTuple<
		BaseTuple<
				IndexSequence< Indices... >
			,	Types...
		>
>
	:	std::true_type
{
};

template<typename T>
struct IsTuple
	:	std::false_type
{
};

template< typename... Types>
struct IsTuple<
	Tuple< Types... >
>
	:	std::true_type
{
};

template<typename T>
struct IsArray
	:	std::false_type
{
};

template< typename T, size_t N>
struct IsArray<
	std::array< T, N >
>
	: std::true_type
{
};

template<typename T>
struct IsPair
	: std::false_type
{
};

template< typename T1, typename T2>
struct IsPair<
	std::pair< T1, T2 >
>
	: std::true_type
{
};

template <template <class> typename>
constexpr bool isAnyOf()
{
	return false;
}

template <template <class> typename Op, typename Head, typename... Tail>
constexpr bool isAnyOf()
{
	return Op<Head>::value || isAnyOf<Op, Tail...>();
}

template< size_t Index, typename Head, typename... Tail >
struct TypeAtIndex
{
	using type = typename TypeAtIndex< Index - 1, Tail... >::type;
};

template< typename Head, typename... Tail >
struct TypeAtIndex< 0, Head, Tail... >
{
	using type = Head;
};

template< size_t Index, typename... Types >
using typeAtIndex_t = typename TypeAtIndex< Index, Types... >::type;

template< typename T >
constexpr int countTypes()
{
	return 0;
}

template< typename T, typename Head, typename... Tail >
constexpr int countTypes()
{
	return (std::is_same_v<T, Head> ? 1 : 0 ) + countTypes< T, Tail... >();
}

template< typename T >
constexpr int getTypeIndex( int )
{
	return -1;
}

template< typename T, typename Head, typename... Tail >
constexpr int getTypeIndex( int _current_index = 0 )
{
	return std::is_same_v< T, Head >
		?	_current_index
		:	getTypeIndex< T, Tail... >( _current_index + 1 );
}

template< typename T >
struct unwrap_refwrapper
{
	using type = T;
};

template< typename T >
struct unwrap_refwrapper< std::reference_wrapper< T > >
{
	using type = T&;
};

template< typename T >
using my_decay_t = typename unwrap_refwrapper< typename std::decay_t< T > >::type;

template <typename T, typename ... Next>
struct variadic_is_same
{
	using type = T;
	static constexpr bool value
	{
		variadic_is_same<Next...>::value &&
		std::is_same<T, typename variadic_is_same<Next...>::type>::value
	};
};

template <typename T>
struct variadic_is_same<T>
{
	using type = T;
	static constexpr bool value = true;
};

template<typename T, typename U>
class isConstructible
{
	static constexpr auto has( T ) -> decltype( std::true_type{} );

	static constexpr auto has( ... ) -> decltype( std::false_type{} );

public:

	static constexpr bool value
	{
		std::is_same_v<
				decltype( has( std::declval< U >() ) )
			,	decltype( std::true_type{} )
		>
	};

};

template<typename T, typename U>
static constexpr bool isConstructible_v = isConstructible< T, U >::value;

template <typename... Types>
struct TypeWrapper{};

template <typename T>
struct TypeWrapper<T>{};

template<
		typename... Types
	,	typename... UTypes
>
constexpr bool areConstructible( TypeWrapper< Types... > &&, TypeWrapper< UTypes... > && )
{
	return ( isConstructible_v< Types, UTypes > && ... );
}

template<
		typename T
	,	typename... UTypes
>
constexpr bool areConstructible( TypeWrapper< T > &&, TypeWrapper< UTypes... > && )
{
	return ( isConstructible_v< T, UTypes > && ... );
}

struct ignore_t
{
	template< typename U >
	ignore_t& operator= ( U&& )
	{
		return *this; // ignores any assignment attempts
	}
} ignore;


} // AZ

#endif // _TUPLEOPERATION_HPP_