#ifndef _TUPLE_HPP_
#define _TUPLE_HPP_
#include "TupleOperations.hpp"
#include "tuple_element.hpp"
#include "BaseTuple.hpp"
#include <functional>
#include <array>
#pragma once

namespace AZ
{

//---------------------------------------------------------------------------//

template< typename... Types >
using base_t = BaseTuple<
		MakeIndexSequence_t< VariadicSize_v< Types... > >
	,	Types...
>;

//---------------------------------------------------------------------------//

template< typename... Types >
class Tuple
	:	public base_t< Types... >
{

	template< size_t... Indices, typename... UTypes >
	static constexpr bool isEqual(
			const Tuple< Types... >& _leftTuple
		,	const Tuple< UTypes... >& _rightTuple
		,	IndexSequence< Indices... >
	);

	template< size_t... Indices, typename... UTypes >
	static constexpr bool isLess( 
			const Tuple< Types... >& _leftTuple
		,	const Tuple< UTypes... >& _rightTuple
		,	IndexSequence< Indices... > 
	);

	template< typename T, size_t N, size_t... Indices >
	constexpr Tuple( const std::array< T, N >& _array, IndexSequence< Indices... > && );

	template< typename T, size_t N, size_t... Indices >
	constexpr Tuple( std::array< T, N >&& _array, IndexSequence< Indices... > && );

	template< typename... UTypes, size_t... Indices >
	constexpr Tuple( const Tuple< UTypes... >& _tuple, IndexSequence< Indices... > && );

	template< typename... UTypes, size_t... Indices >
	constexpr Tuple( Tuple< UTypes... >&& _tuple, IndexSequence< Indices... > && );

	template< typename Tuple1, size_t... Indices >
	void copy( Tuple1&& _from, IndexSequence< Indices... >&& );

//---------------------------------------------------------------------------//

public:

	constexpr Tuple() noexcept = default;

	template<
			typename... UTypes
		,	typename = typename std::enable_if_t<
					VariadicSize_v< UTypes... > == VariadicSize_v< Types... >
				&&	areConstructible( 
							TypeWrapper< Types... > {}
						,	TypeWrapper< UTypes&&... > {}
					)
			>
	>
	constexpr Tuple( UTypes&&... _args );

	template<
			typename T1
		,	typename T2
		,	typename = typename std::enable_if_t<
					VariadicSize_v< Types... > == 2
				&&	areConstructible(
							TypeWrapper< T1, T2 > {}
						,	TypeWrapper< const Types&... > {}
					)
			>
	>
	constexpr Tuple( const std::pair<T1, T2>& _pair );

	template<
			typename T1
		,	typename T2
		,	typename = typename std::enable_if_t<
					VariadicSize_v< Types... > == 2
				&&	areConstructible(
							TypeWrapper< T1, T2 > {}
						,	TypeWrapper< Types&&... > {}
					)
			>
	>
	constexpr Tuple( std::pair<T1, T2>&& _pair );

	template<
			typename T
		,	size_t N
		,	typename = typename std::enable_if_t<
					VariadicSize_v< Types... > == N
				&&	areConstructible(
							TypeWrapper< T > {}
						,	TypeWrapper< const Types&... >{} 
					)
			>
	>
	constexpr Tuple( const std::array< T, N >& _array );

	template<
			typename T
		,	size_t N
		,	typename = typename std::enable_if_t<
					VariadicSize_v< Types... > == N
				&&	areConstructible( TypeWrapper< T > {}, TypeWrapper< Types&&... > {} )
			>
	>
	constexpr Tuple( std::array< T, N >&& _array );

	Tuple( const Tuple& _tuple ) = default;

	Tuple &operator= ( const Tuple& _tuple );

	template<
			typename... UTypes
		,	typename = typename std::enable_if_t<
					VariadicSize_v< Types... > == VariadicSize_v< UTypes... >
				&&	areConstructible(
							TypeWrapper< Types... > {}
						,	TypeWrapper< const UTypes&... > {}
					)
			>
	>
	constexpr Tuple( const Tuple<UTypes...> & _tuple );

	template< typename... UTypes >
	Tuple &operator = ( const Tuple<UTypes...> & _tuple );

	Tuple( Tuple&& _tuple ) = default;

	Tuple &operator = ( Tuple&& _tuple ) = default;

	template<
			typename... UTypes
		,	typename = typename std::enable_if_t<
					VariadicSize_v< Types... > == VariadicSize_v< UTypes... >
				&&	areConstructible(
							TypeWrapper< Types... > {}
						,	TypeWrapper< UTypes&&... > {}
					)
			>
	>
	constexpr Tuple( Tuple<UTypes...>&& _tuple );

//---------------------------------------------------------------------------//

	template< typename... UTypes >
	constexpr bool operator == ( const Tuple< UTypes... >& _tuple ) const;

	template< typename... UTypes >
	constexpr bool operator != ( const Tuple< UTypes... >& _tuple ) const;

	template< typename... UTypes >
	constexpr bool operator < ( const Tuple< UTypes... >& _tuple ) const;

	template< typename... UTypes >
	constexpr bool operator <= ( const Tuple< UTypes... >& _tuple ) const;

	template< typename... UTypes >
	constexpr bool operator > ( const Tuple< UTypes... >& _tuple ) const;

	template< typename... UTypes >
	constexpr bool operator >= ( const Tuple< UTypes... >& _tuple ) const;

	void swap( Tuple& _tuple ) noexcept;

//---------------------------------------------------------------------------//

};

//---------------------------------------------------------------------------//

template<typename... Types>
constexpr Tuple<my_decay_t< Types >...> makeTuple( Types && ...args )
{
	return Tuple<my_decay_t< Types >...>( std::forward<Types>( args )... );
}

template<typename... Types>
constexpr Tuple<Types&&...> forwardAsTuple( Types && ...args )
{
	return Tuple< Types&&... >( std::forward<Types>( args )... );
}

template< typename >
struct TupleSize;

template< typename... Types >
struct TupleSize< Tuple< Types... > >
	:	std::integral_constant< size_t, VariadicSize_v< Types... > >
{
};

template< typename T >
static constexpr size_t TupleSize_v = TupleSize< T >::value;

template< typename... Types >
constexpr Tuple< Types&... > tie( Types&... _args )
{
	return Tuple< Types&... >( _args... );
}


//---------------------------------------------------------------------------//

template< typename... Types >
template< typename... UTypes
	,	typename = typename std::enable_if_t<
				VariadicSize_v<UTypes...> == VariadicSize_v<Types...>
			&&	areConstructible(
						TypeWrapper< Types... > {}
					,	TypeWrapper< UTypes&&... > {}
				)
		>
>
constexpr Tuple< Types... >::Tuple( UTypes&&... _args )
	:	base_t< Types... >( std::forward< UTypes >( _args )... )
{
}

template< typename... Types >
template<
		typename... UTypes
	,	typename = typename std::enable_if_t<
				VariadicSize_v< Types... > == VariadicSize_v< UTypes... >
			&&	areConstructible(
						TypeWrapper< Types... > {}
					,	TypeWrapper< const UTypes&... > {}
				)
		>
>
constexpr Tuple< Types... >::Tuple( const Tuple< UTypes... >& _tuple )
	:	Tuple< Types... >( _tuple, MakeIndexSequence_t< VariadicSize_v< Types... > > {} )
{
}

template< typename... Types >
template<
		typename... UTypes
	,	typename = typename std::enable_if_t<
			VariadicSize_v< Types... > == VariadicSize_v< UTypes... >
		&&	areConstructible(
					TypeWrapper< Types... > {}
				,	TypeWrapper< UTypes&&... > {}
			)
		>
>
constexpr Tuple<Types...>::Tuple( Tuple< UTypes... >&& _tuple )
	:	Tuple< Types... >( std::move( _tuple ), MakeIndexSequence_t< VariadicSize_v< Types... > > {} )
{
}

template< typename... Types >
template<
		typename T1
	,	typename T2
	,	typename = typename std::enable_if_t<
				VariadicSize_v< Types... > == 2
			&&	areConstructible(
						TypeWrapper< T1, T2 > {}
					,	TypeWrapper< const Types&... > {}
				)
		>
>
constexpr Tuple< Types... >::Tuple( const std::pair< T1, T2 >& _pair )
	:	base_t< Types... >( _pair.first, _pair.second )
{
}

template< typename... Types >
template<
		typename T1
	,	typename T2
	,	typename = typename std::enable_if_t<
				VariadicSize_v< Types... > == 2
			&&	areConstructible(
						TypeWrapper< T1, T2 > {}
					,	TypeWrapper< Types&&... > {}
				)
		>
>
constexpr Tuple< Types... >::Tuple( std::pair< T1, T2 >&& _pair )
	:	base_t< Types... >( std::move( _pair.first ), std::move( _pair.second ) )
{
}

template< typename... Types >
template<
		typename T
	,	size_t N
	,	typename = typename std::enable_if_t<
				VariadicSize_v< Types... > == N
			&&	areConstructible( TypeWrapper< T > {}, TypeWrapper< const Types&... > {} )
		>
>
constexpr Tuple< Types... >::Tuple( const std::array< T, N >& _array )
	: Tuple< Types... >( _array, MakeIndexSequence_t< N > {} )
{
}

template< typename... Types >
template<
		typename T
	,	size_t N
	,	typename = typename std::enable_if_t<
				VariadicSize_v< Types... > == N
			&&	areConstructible( TypeWrapper< T > {}, TypeWrapper< const Types&... > {} )
		>
>
constexpr Tuple< Types... >::Tuple( std::array< T, N >&& _array )
	:	Tuple< Types... >( std::move( _array ), MakeIndexSequence_t< N > {} )
{
}

template< typename... Types >
template< typename T, size_t N, size_t... Indices >
constexpr Tuple< Types... >::Tuple( const std::array< T, N >& _array, IndexSequence< Indices... >&& )
	:	base_t< Types... >( _array[Indices]... )
{
}

template< typename... Types >
template< typename T, size_t N, size_t... Indices >
constexpr Tuple< Types... >::Tuple( std::array< T, N >&& _array, IndexSequence< Indices... >&& )
	:	base_t< Types... >( std::move( _array[Indices] )... )
{
}

template< typename... Types >
template< typename... UTypes, size_t... Indices >
constexpr Tuple< Types... >::Tuple( const Tuple< UTypes... > & _tuple, IndexSequence< Indices... > && )
	:	Tuple< Types... >( get< Indices >( _tuple )... )
{
}

template< typename... Types >
template< typename... UTypes, size_t... Indices >
constexpr Tuple< Types... >::Tuple( Tuple< UTypes... > && _tuple, IndexSequence< Indices... > && )
	: Tuple< Types... >( std::move( get< Indices >( _tuple ) )... )
{
}

//---------------------------------------------------------------------------//

template< size_t Index, typename... Types >
typeAtIndex_t< Index, Types... >& get( Tuple< Types... > &_tuple )
{
	tuple_element< Index, typeAtIndex_t< Index, Types... > > & base = _tuple;
	return base.m_value;
}

template< size_t Index, typename... Types >
const typeAtIndex_t< Index, Types... >& get( const Tuple< Types... > &_tuple )
{
	const tuple_element< Index, typeAtIndex_t< Index, Types... > > & base = _tuple;
	return base.m_value;
}

template< size_t Index, typename... Types >
std::remove_reference_t< typeAtIndex_t< Index, Types... > >&& get( Tuple< Types... > &&_tuple )
{
	tuple_element< Index, typeAtIndex_t< Index, Types... > > & base = _tuple;
	return std::move( base.m_value );
}

template< typename T, typename... Tail >
T & get( Tuple< Tail... > & _tuple )
{
	static_assert( countTypes< T, Tail... >() == 1, "T must appear exactly once in ...Types" );
	return get< getTypeIndex< T, Tail... >() >( _tuple );
}

template< typename... Types >
template< typename... UTypes >
constexpr bool Tuple< Types... >::operator==( const Tuple< UTypes... > & _tuple ) const
{
	constexpr size_t countTypes = VariadicSize_v< Types... >;
	constexpr size_t countOtherTypes = VariadicSize_v< UTypes... >;

	if ( countTypes != countOtherTypes )
		return false;

	return isEqual( *this, _tuple, MakeIndexSequence_t< countTypes >{} );
}

template<typename ...Types>
template<typename ...UTypes>
constexpr bool Tuple< Types... >::operator!=( const Tuple< UTypes... >& _tuple ) const
{
	return !(*this == _tuple);
}

template<typename ...Types>
template<typename ...UTypes>
constexpr bool Tuple< Types... >::operator< ( const Tuple< UTypes... >& _tuple ) const
{
	static_assert( VariadicSize_v< Types... > == VariadicSize_v< UTypes... >, "Amount of types should be equal" );
	return isLess( *this, _tuple, MakeIndexSequence_t< VariadicSize_v< Types... > >{} );
}

template<typename ...Types>
template<typename ...UTypes>
constexpr bool Tuple< Types... >::operator<= ( const Tuple< UTypes... >& _tuple ) const
{
	return !( _tuple < *this );
}

template<typename ...Types>
template<typename ...UTypes>
constexpr bool Tuple< Types... >::operator> ( const Tuple< UTypes... >& _tuple ) const
{
	return _tuple < *this;
}

template<typename ...Types>
template<typename ...UTypes>
constexpr bool Tuple< Types... >::operator>= ( const Tuple< UTypes... >& _tuple ) const
{
	return !( *this < _tuple );
}

template<typename ...Types>
template<size_t... Indices, typename ...UTypes>
constexpr bool Tuple<Types...>::isEqual(
		const Tuple<Types...>& _leftTuple
	,	const Tuple<UTypes...>& _rightTuple
	,	IndexSequence< Indices... > 
)
{
	return ( ( get< Indices >( _leftTuple ) == get< Indices >( _rightTuple ) ) && ... );
}

template<typename ...Types>
template<size_t... Indices, typename ...UTypes>
constexpr bool Tuple<Types...>::isLess( const Tuple<Types...>& _leftTuple, const Tuple<UTypes...>& _rightTuple, IndexSequence< Indices... > )
{
	return ( 
			( get< Indices >( _leftTuple ) < get< Indices >( _rightTuple ) 
		||	!( get< Indices >( _rightTuple ) < get< Indices >( _leftTuple ) ) ) && ...
	);
}

//---------------------------------------------------------------------------//

template< typename... Types >
template< typename... UTypes >
Tuple< Types... >& Tuple< Types... >::operator= ( const Tuple<UTypes...> & _tuple )
{
	copy( _tuple, MakeIndexSequence_t< VariadicSize_v< Types... > >{} );
	return ( *this );
}

template< typename... Types >
Tuple< Types... >& Tuple< Types... >::operator= ( const Tuple< Types... > & _tuple )
{
	copy( _tuple, MakeIndexSequence_t< VariadicSize_v< Types... > >{} );
	return ( *this );
}

template<typename... Types>
template< typename Tuple1, size_t... Indices>
void Tuple<Types...>::copy( Tuple1&& _from, IndexSequence<Indices...>&& )
{
	( ( get< Indices >( *this ) = get< Indices >( _from ) ), ... );
}

template<typename... Types>
void Tuple< Types... >::swap( Tuple& _tuple ) noexcept
{
//	static_assert( VariadicSize_v< Types... > == TupleSize_v< decltype( _tuple ) > );
}

//---------------------------------------------------------------------------//

//template <typename F, typename T, size_t... Indices>
//void tupleForeachImpl( F&& f, T&& t, IndexSequence<Indices...> )
//{
//	auto unused = { true, ( f( Get<Indices>( std::forward<T>( t ) ) ), void(), true )... };
//}
//
//template <typename F, typename T>
//void tupleForeach( F&& f, T&& t )
//{
//	tupleForeachImpl( forward<F>( f ), forward<T>( t ), std::make_index_sequence<std::tuple_size<std::remove_reference_t<T>>::value>() );
//}

template< typename Tuple1, size_t... Indices1, typename Tuple2, size_t... Indices2 >
auto tupleCatHelper(
		Tuple1&& _tuple1
	,	Tuple2&& _tuple2
	,	IndexSequence< Indices1... >
	,	IndexSequence< Indices2... >
)
{
	return makeTuple(
			get< Indices1 >( std::forward< Tuple1 >( _tuple1 ) )...
		,	get< Indices2 >( std::forward< Tuple2 >( _tuple2 ) )...
	);
}

template< typename Tuple1, typename Tuple2 >
auto tupleCat2( Tuple1&& _tuple1, Tuple2&& _tuple2 )
{
	return tupleCatHelper(
			std::forward< Tuple1 >( _tuple1 )
		,	std::forward< Tuple2 >( _tuple2 )
		,	MakeIndexSequence_t< TupleSize_v< std::decay_t< Tuple1 > > > {}
		,	MakeIndexSequence_t< TupleSize_v< std::decay_t< Tuple2 > > > {}
	);
}

template< typename HeadTuple >
HeadTuple&& tupleCat( HeadTuple&& _tuple )
{
	return std::forward< HeadTuple >( _tuple );
}

template< typename HeadTuple1, typename HeadTuple2, typename... TupleTail >
auto tupleCat( HeadTuple1&& _tuple1, HeadTuple2&& _tuple2, TupleTail&&... _tail )
{
	return tupleCat(
			tupleCat2(
					std::forward< HeadTuple1 >( _tuple1 )
				,	std::forward< HeadTuple2 >( _tuple2 )
			)
		,	std::forward< TupleTail >( _tail )...
	);
}

//---------------------------------------------------------------------------//

} // AZ

#endif // _TUPLE_HPP_