#include "testslib.hpp"
#include <iostream>
#include <tuple>
#include <array>
#include "Tuple.hpp"


DECLARE_TUPLE_TEST( test_simple_ctors )
{
	AZ::Tuple<int, int> t0;
	AZ::Tuple<int, float> t1( 3, 3.14f ); // exact match
	AZ::Tuple<long long, double> t2( 3, 3.14f ); // widening conversions
	AZ::Tuple<std::string> t3{ "Hello, World" }; // string construction from char const[]


	struct MyStruct {};
	AZ::Tuple<MyStruct, int, MyStruct, long long, unsigned char, double, const char *> t4;

	assert( AZ::TupleSize< decltype( t4 ) >::value == 7 );

	int x = 42;
	int y = 43;
	AZ::Tuple<int&> t5( x );
	AZ::Tuple<int*> t6( &x );
	AZ::Tuple<int&&> t7 ( std::move( y ) );

	AZ::Tuple< int, int > t9( 10, 20 );
	AZ::Tuple< AZ::Tuple< int, int > > t8( t9 );
	assert( AZ::get< 0 >( AZ::get< 0 >( t8 ) ) == 10 );
}

DECLARE_TUPLE_TEST( test_pair_and_array_ctors )
{
	std::pair<std::string, int> p1 = std::make_pair( "one", 1 );
	AZ::Tuple< int, double > t1( std::make_pair( 52, 3.5 ) );
	AZ::Tuple< std::string, int > t2( p1 );

	std::array< int, 1 > a1{ { 1 } };
	AZ::Tuple< int > t3( a1 );
	AZ::Tuple< int, int > t12( std::array< int, 2 > {1, 2} );
	assert( AZ::get< 1 >( t12 ) == 2 );

	const std::array< int, 5 >& a2{ { 1, 2, 3, 4, 5 } };
	AZ::Tuple< int, int, int, int, int > t4( a2 );
	assert( AZ::get< 4 >( t4 ) == 5 );

	const std::array< std::array< int, 1 >, 1 > &a3{ { std::array< int, 1 > {1} } };
	AZ::Tuple< std::array< std::array< int, 1 >, 1 > > t5( a3 ); // ctor with UTypes...
	assert( AZ::get< 0 >( t5 ).at( 0 ).at( 0 ) == 1 );

	std::pair< std::pair< double, char >, int > p2 = std::make_pair( std::make_pair( 1.2, 'a' ), 3 );
	AZ::Tuple< std::pair< std::pair< double, char >, int > > t6( p2 ); // ctor with UTypes...

	std::array< int, 2 > a4 { {1, 2} };
	//AZ::Tuple< int, int, int > t7( a4, 3 ); // shouldn't compile

	std::pair< std::pair< double, char >, int > p3 = std::make_pair( std::make_pair( 1.2, 'a' ), 3 );
	AZ::Tuple< std::pair< double, char >, int > t8( p3 ); // ctor with pair

	const std::pair< std::pair< double, char >, std::pair< double, char > > &p4 =
		std::make_pair( std::make_pair( 1.2, 'a' ), std::make_pair( 2.2, 'b' ) );
	AZ::Tuple< std::pair< double, char >, std::pair< double, char > > t9( p4 ); // ctor with pair

	std::array< std::array< int, 2 >, 2 > a5{ { { 1, 2 }, { 3, 4 } } };
	AZ::Tuple< std::array< int, 2 >, std::array< int, 2 > > t10( std::move( a5 ) ); // ctor with array
	assert( a5.at(0).at(0) == 1);
	assert( AZ::get< 0 >( t10 ).at(0) == 1 );

	std::array< int, 4 > a6{ { 1, 2, 3, 4 } };
	AZ::Tuple< double, int, short, long > t11( a6 ); // conversion is OK
	assert( AZ::get< double >( t11 ) == 1.0 );
	assert( AZ::get< int >( t11 ) == 2 );
	assert( AZ::get< short >( t11 ) == 3 );
	assert( AZ::get< long >( t11 ) == 4 );

	std::pair< int, double > p5( 3, 3.4 );
	AZ::Tuple< short, int > t13( p5 ); // conversion is OK
	assert( AZ::get< short >( t13 ) == 3 ); 
	assert( AZ::get< int >( t13 ) == 3 );

	struct Base {};
	
	struct Derrived1 : public Base{};

	struct Derrived2 : public Derrived1{};

	AZ::Tuple< Base, Derrived1 > t14( Derrived2{}, Derrived2{} );
	//AZ::Tuple< Derrived2, Derrived1 > t15( Derrived1{}, Base{} ); // SHOULD NOT compile
}

DECLARE_TUPLE_TEST(test_copy_and_move_semantic)
{
	AZ::Tuple<int, std::string> t1(3, "some");
	AZ::Tuple<int, std::string> t2( t1 );

	auto t3 = t1;

	AZ::Tuple<short, char> t6;
	AZ::Tuple<short, char> t4(3, 'a');
	AZ::Tuple<short, char> t5 = std::move(t4);
	t2 = t3;
	t6 = std::move(t5);

	AZ::Tuple< int, int > t7( 3, 5 );
	AZ::Tuple< AZ::Tuple< int, int > > t8( t7 ); // not copy ctor
	assert( AZ::get< 0 > ( AZ::get< 0 >( t8 ) ) == 3 );

	AZ::Tuple< int, float > t9( 3, 4.5f );
	AZ::Tuple< short, double > t10( t9 );
	assert( AZ::get< short >( t10 ) == 3 );
	assert( AZ::get< double >( t10 ) == 4.5 );

	AZ::Tuple< std::string, long long > t11( "some", 25 );
	AZ::Tuple< std::string, int > t12( std::move( t11 ) );
	assert( AZ::get< std::string >( t12 ) == "some" );
	assert( AZ::get< int >( t12 ) == 25 );

	AZ::Tuple< int > t13( 10 );
	AZ::Tuple< short > t14( t13 );
	assert( AZ::get< short >( t14 ) == 10 );
	assert( AZ::get< int >( t13 ) == 10 );

	struct X
	{
		X()
		{}
		X( const X & ) = delete;
		X & operator = ( const X & ) = delete;
	};

	X x;
	AZ::Tuple< X& > t15 (x);
	auto & x1 = AZ::get< 0 >( t15 );

	int a;
	std::string b;
	AZ::Tuple< int, std::string> t16( 34, "some" );
	AZ::Tuple< int&, std::string&> t17( a, b );

	t17 = t16;

	AZ::Tuple< int > t18 = AZ::makeTuple( 3 );
	AZ::Tuple< int & > t19( a );
	t19 = t18;

	assert( AZ::get< int & >( t19 ) == 3 );
}

DECLARE_TUPLE_TEST( test_get )
{
	AZ::Tuple<int, std::string> t1( 3, "some" );
	
	std::string &value = AZ::get<1>(t1);
	assert( value == "some" );

	value = "another";

	assert( AZ::get<1>(t1) == "another" );
	assert( AZ::get<0>(t1) == 3 );

	int a = AZ::get<int>(t1);
	std::string b = AZ::get<std::string>(t1);
	//auto c = AZ::Get<short>(t1); // shouldn't compile

	assert( b == "another" );
	assert( a == 3 );

	AZ::Tuple<int, std::string, double, int> t2( 3, "some", 3.14, 5 );

	assert( AZ::get<1>( t2 ) == "some" );
	assert( AZ::get<0>( t2 ) == 3 );
	assert( AZ::get<2>( t2 ) == 3.14 );

	assert( AZ::get< double >( t2 ) == 3.14 );
	//assert( AZ::Get< int >( t2 ) == 3 ); // static_assert fails

	int x = 1;
	AZ::Tuple< int& > t3( x );
	int & v = AZ::get< int & >( t3 );
	v = 2;
	assert( AZ::get< 0 >( t3 ) == 2 );

	//AZ::Get< 1 >( t3 ); // shouldn't compile

	int x1 = 42;
	AZ::Tuple<int&> t( x1 );
	AZ::get<0>( std::move( t ) ); // returns int&&
}

DECLARE_TUPLE_TEST( test_operators )
{
	AZ::Tuple< int, double, std::string > t1( 3, 5.6, "abc" );
	AZ::Tuple< int, double, std::string > t2( 3, 5.6, "abc" );
	AZ::Tuple< int, double, const char * > t3( 3, 5.6, "abc" );
	AZ::Tuple< int, double, std::string > t4( 3, 5.7, "abc" );

	assert( t1 == t2 );
	assert( t1 == t3 );
	assert( t3 < t4 );
	assert( t4 > t3 );
	assert( t4 >= t3 );
	assert( t3 <= t4 );
}

DECLARE_TUPLE_TEST( test_make_tuple )
{
	auto t1 = AZ::makeTuple( 3, "some" );
	assert( AZ::get< int >( t1 ) == 3 );
	assert( AZ::TupleSize< decltype( t1 ) >::value == 2 );

	AZ::Tuple< int, std::string > t2;
	int a = 3;
	const std::string &b = "abc";
	t2 = AZ::makeTuple( a, b ); // ignores any references
	assert( AZ::get< int >( t2 ) == 3 );
	assert( AZ::get< std::string >( t2 ) == "abc" );

	int a1 = 3;
	std::string b1 = "abc";
	auto t3 = AZ::forwardAsTuple( a1, std::move( b1 ) ); // forwards with saving references
	assert( AZ::get< int& >( t3 ) == 3 );
	assert( AZ::get< std::string&& >( t3 ) == "abc" );

	struct Base {};

	struct Derrived1 : public Base{};

	struct Derrived2 : public Derrived1{};

	AZ::Tuple< Base, Derrived1 > t4;
	t4 = AZ::makeTuple( Derrived1{}, Derrived2{} );
}

DECLARE_TUPLE_TEST( test_tie )
{
	int x = 0;
	std::string y = "";
	AZ::Tuple< int, std::string> t1( 34, "some" );
	AZ::Tuple< int&, std::string&> t2( x, y );

	int a;
	std::string b;

	AZ::tie( a, b ) = t1;

	assert( a == 34 );
	assert( b == "some" );

	AZ::tie( a, b ) = t2;

	assert( a == 0 );
	assert( b == "" );

	AZ::Tuple< int, double, std::string, short > t3( 3, 4.5, "bar", 1 );

	AZ::tie( a, AZ::ignore, b, AZ::ignore ) = t3;

	assert( a == 3 );
	assert( b == "bar" );
}

DECLARE_TUPLE_TEST( test_tuple_cat )
{
	auto t1 = AZ::tupleCat( AZ::makeTuple( 1 ), AZ::makeTuple( 2 ) ); // tuple<int, int>(1, 2)
	auto t2 = AZ::tupleCat( t1, AZ::makeTuple( 3 ), AZ::makeTuple( 4 ) );
	auto t3 = AZ::tupleCat( t1, t1, t2, t2 );
	// t3 is a tuple of 12 ints: 1, 2, 1, 2, 1, 2, 3, 4, 1, 2, 3, 4

	assert( AZ::TupleSize_v< decltype( t3 ) > == 12 );
}

DECLARE_TUPLE_TEST( boost_tests )
{
//	AZ::Tuple<double&> t; // SHOULD NOT compile: reference must be 

	double d = 5;
	AZ::Tuple<double&> t( d ); // ok

//	AZ::Tuple<double&>( d + 3.14 ); // SHOULD NOT compile: cannot initialize 

	AZ::Tuple<const double&> t1( d + 3.14 ); // ok, but dangerous

	class Y
	{
	public:
		Y() = default;
		Y( const Y& ) = delete;
	};

	char a[10];

//	AZ::Tuple<char[10], Y> t( a, Y() ); // SHOULD NOT compile, neither arrays nor Y can be copied

	AZ::Tuple<char[10], Y> t0; // ok

	Y y;
	AZ::Tuple< char(&)[10], Y& > t2( a, y );


	struct A
	{
		bool operator==( const A & ) const { return true; }
	};
	class B : public A {};
	struct C { C(){} C( const B& ){} };
	struct D { operator C() const{ return C(); } };
	AZ::Tuple<char, B*, B, D> t4;

	AZ::Tuple<int, A*, C, C> t5( t4 ); // ok
	t5 = t4; // ok

	AZ::Tuple<std::string, int, A> t6( std::string( "same?" ), 2, A() );
	AZ::Tuple<std::string, long, A> t7( std::string( "same?" ), 2, A() );
	AZ::Tuple<std::string, long, A> t8( std::string( "different" ), 3, A() );

	assert( t6 == t7 ); // true
	assert( t6 != t8 ); // false, does not print "All the..."

	int i; char c; double d1;
	AZ::tie( i, c, d1 ) = AZ::makeTuple( 1, 'a', 5.5 );

	assert( i == 1 );
	assert( c == 'a' );
	assert( d1 == 5.5 );

	AZ::Tuple< int , char > t9 = std::make_pair( 1, 'a' );
	AZ::tie( i, c ) = AZ::makeTuple( 1, 'a' ); // ok
}

DECLARE_TUPLE_TEST( test_swap )
{
	AZ::Tuple< int, std::string, double > t0( 1, "one", 1.1 );
	AZ::Tuple< int, std::string, double > t1( 2, "two", 2.2 );

	t0.swap( t1 );
}