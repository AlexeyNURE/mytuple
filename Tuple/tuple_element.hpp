#ifndef _TUPLE_ELEMENT_HPP_
#define _TUPLE_ELEMENT_HPP_

namespace AZ
{

template< size_t Index, typename T >
struct tuple_element
{
	T m_value;

public:
	explicit tuple_element( std::remove_reference_t<T> const & _value )
		: m_value( _value )
	{}

	explicit tuple_element( std::remove_reference_t<T>&& _value )
		: m_value( std::move( _value ) )
	{}

	template< typename U >
	explicit tuple_element( U&& _value );

	tuple_element() = default;

	tuple_element( const tuple_element& ) = default;

	tuple_element &operator = ( const tuple_element& ) = default;

	tuple_element( tuple_element&& ) = default;

	tuple_element &operator = ( tuple_element&& ) = default;
};

template<size_t Index, typename T>
template<typename U>
tuple_element<Index, T>::tuple_element( U&& _value )
	:	m_value( std::forward<U>( _value ) )
{
}

} // AZ

#endif // _TUPLE_ELEMENT_HPP_