#ifndef _BASETUPLE_HPP_
#define _BASETUPLE_HPP_
#include "TupleOperations.hpp"
#include "tuple_element.hpp"
#include <iostream>
#include <type_traits>

namespace AZ
{

template< size_t... Indices, typename ...Types >
struct BaseTuple<
		IndexSequence< Indices... >
	,	Types...
>
	:	public tuple_element< Indices, Types >...
{
public:
	BaseTuple() = default;

	template< typename... UTypes >
	constexpr explicit BaseTuple( UTypes&&... _args );

	BaseTuple( const BaseTuple& _element ) = default;

	BaseTuple &operator = ( const BaseTuple& _element ) = default;

	BaseTuple( BaseTuple&& _element ) = default;

	BaseTuple &operator = ( BaseTuple&& _element ) = default;
};

template< size_t ...Indices, typename ...Types >
template< typename... UTypes >
constexpr BaseTuple< IndexSequence< Indices... >, Types... >::BaseTuple( UTypes&&... _args )
	:	tuple_element< Indices, Types >( std::forward<UTypes>( _args ) )...
{
}

} // AZ

#endif // _BASETUPLE_HPP_